class Api::V1::UsersController < Api::V1::BaseController
  before_action :authenticate_with_token!, only: [:update, :destroy]
  respond_to :json

  def show
    begin
      @user = User.find(params[:id])
      respond_with @user
    rescue Exception => e
      logger.info "============== User not found #{params[:id].inspect} - Message: #{e.message} =============="
      head 404
    end
  end

  def create
    user = User.new(user_params)
    if user.save
      render json: user, status: 201
    else
      render json: { errors: user.errors }, status: 422
    end
  end

  def update
    user = current_user
    if user.update_attributes(user_params)
      render json: user, status: 200
    else
      render json: { errors: user.errors }, status: 422
    end
  end

  def destroy
    user = current_user
    if user.destroy
      head 204
    else
      render json: { errors: user.errors }, status: 422
    end
  end

  private

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end
end
