FactoryBot.define do
  sequence :email do |n|
    "admin#{n}@task_manager.com"
  end
  sequence :name do |n|
    "Usuario #{n}"
  end
end
