require 'rails_helper'

RSpec.describe 'Sessions API', type: :request do
  let!(:user) { create(:user) }
  let!(:auth_data) { user.create_new_auth_token }
  let(:headers) do
    {
      'Accept' => 'application/vnd.taskmanager.v2',
      'Content-Type' => Mime[:json].to_s,
      'access-token' => auth_data['access-token'],
      'client' => auth_data['client'],
      'uid' => auth_data['uid']
    }
  end
  before { host! "api.taskmanager.dev" }

  describe 'POST /auth/sign_in' do
    before do
      post '/auth/sign_in', params: credentials.to_json, headers: headers
    end

    context 'when the credentials are correct' do
      let(:credentials) do
        {
          email: user.email,
          password: 'asdqwe123'
        }
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns the authentication data and headers' do
        expect(response.headers).to have_key('access-token')
        expect(response.headers).to have_key('uid')
        expect(response.headers).to have_key('client')
      end
    end

    context 'when the credentials are incorrects' do
      let(:credentials) do
        {
          email: user.email,
          password: 'invalid password'
        }
      end

      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end

      it 'returns user json with auth token' do
        expect(json_body).to have_key('errors')
      end
    end
  end

  describe 'DELETE /auth/sign_out' do
    let(:auth_token){ user.auth_token }

    before do
      delete '/auth/sign_out', params: {}, headers: headers
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'expires the user auth_token' do
      user.reload
      expect(user).not_to be_valid_token(auth_data['access-token'], auth_data['client'])
    end
  end
end